証明書は OpenSSL で作ったオレオレ証明書です。手順書。

- 秘密鍵作成

```
openssl genrsa 2024 > server.key
```

- 中間証明書作成

```
openssl req -new -key server.key > server.csr
```

- サーバ証明書作成

```
openssl x509 -req -days 3650 -signkey server.key < server.csr > server.crt
```
