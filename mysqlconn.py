import mysql.connector


class connector(object):
    def __init__(self, account):
        self.account = account

    def __enter__(self):

        self.connect = mysql.connector.connect(
            database=self.account["DB_NAME"],
            host=self.account["DB_HOST"],
            user=self.account["DB_USER"],
            passwd=self.account["DB_PASSWORD"],
            port=self.account["DB_PORT"],
            charset="utf8")
        print("接続成功" if self.connect.is_connected() else "接続失敗")
        return self.connect

    def __exit__(self, exception_type, exception_value, traceback):
        self.connect.close()
