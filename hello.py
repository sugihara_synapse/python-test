from flask import Flask, request, render_template
import subprocess
import glob
import hashlib
import pathlib

from datetime import datetime
app = Flask(__name__)


@app.route('/')
def hello():
    files = glob.glob("static/image/*")
    return render_template("index.html", images=files)


@app.route('/certs', methods=['GET'])
def getcerts():
    return render_template("certs.html")


@ app.route('/certs', methods=['POST'])
def postcerts():
    error = ""
    if 'domain' not in request.form:
        error += "ドメイン未入力<br/>"
    if 'crt' not in request.files:
        error += "サーバ証明書ファイル未指定<br/>"
    if 'ca' not in request.files:
        error += "中間証明書ファイル未指定<br/>"
    if 'key' not in request.files:
        error += "秘密鍵ファイル未指定<br/>"
    if len(error) > 0:
        return error

    path = "static/certs/www."
    domain = request.form["domain"]
    crt = request.files['crt']
    ca = request.files['ca']
    key = request.files['key']

    crt_path = path + domain + ".crt"
    ca_path = path + domain + ".ca"
    key_path = path + domain + ".key"

    crt.save(crt_path)
    ca.save(ca_path)
    key.save(key_path)

    cmdlog = "アップロード完了\n\n"
    cmd = "openssl x509 -noout -modulus -in " + crt_path + " | md5sum"
    process_1 = (subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                  shell=True).communicate()[0]).decode('utf-8')

    cmdlog += cmd + "\n"
    cmdlog += process_1 + "\n"

    cmd = "openssl rsa -noout -modulus -in " + key_path + " | md5sum"
    process_2 = (subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                  shell=True).communicate()[0]).decode('utf-8')

    cmdlog += cmd + "\n"
    cmdlog += process_2 + "\n"

    if process_1 == process_2:
        cmdlog += "サーバ証明書と秘密鍵の整合を確認しました。"
    else:
        cmdlog += "サーバ証明書と秘密鍵の整合が確認できません。"

    return render_template("certs_r.html", result=cmdlog, crt_path=crt_path, ca_path=ca_path, key_path=key_path)


@ app.route('/ls')
def ls():
    cmd = 'ls -al'
    process = (subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                shell=True).communicate()[0]).decode('utf-8')

    return 'Command: <pre>' + cmd + '</pre><pre>' + process + '</pre>です'

# ファイルアップロード


@ app.route('/upload', methods=['POST'])
def upload():
    if request.method == 'POST':
        if 'file' not in request.files:
            return 'ファイル未指定'
        fs = request.files['file']

        app.logger.info('file_name={}'.format(fs.filename))
        app.logger.info('content_type={} content_length={}, mimetype={}, mimetype_params={}'.format(
            fs.content_type, fs.content_length, fs.mimetype, fs.mimetype_params))
        extention = pathlib.Path(fs.filename).suffix
        path = 'static/image/' + \
            str(datetime.now().strftime("%Y%m%d_%H%M%S")) + extention
        # ファイルを保存
        fs.save(path)
        ans = '''
        <div>ファイルをアップロードしました。</div>
        <img src="/{}">
        </form>
    '''.format(path)
    return ans


@ app.route('/bin/<filename>')
# ファイルを表示する
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
