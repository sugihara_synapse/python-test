from mysqlconn import connector
import setting

seeddir = "seed\\"


def seeding(filename)
    with connector(setting.mysql) as connect:
        with connect.cursor(dictionary=True) as cursor:
            cursor.execute(getSeed(filename))
            cursor.commit()
    return


def getSeed(filename)
    f = open(seeddir + filename, 'r')
    data = f.read()
    f.close()
    return data


seeding("test.txt")
