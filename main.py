from flask import Flask, request, render_template
import mysql.connector as mydb
import setting

app = Flask(__name__)


@app.route('/')
def hello():
    conn = mydb.connect(
        user=setting.DB_USER,
        port=setting.DB_PORT,
        passwd=setting.DB_PASSWORD,
        host=setting.DB_HOST,
        database=setting.DB_NAME)

    connection_succeeded = "接続成功" if conn.is_connected() else "接続失敗"
    print(connection_succeeded)
    cur = conn.cursor()

    cur.execute(
        "SELECT DISTINCT ProcessType FROM ServerTask order by ProcessType;")

    rawdata = cur.fetchall()

    cur.close
    conn.close

    return render_template("view.html", data=rawdata)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
