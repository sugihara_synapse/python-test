import os
from os.path import join, dirname
from dotenv import load_dotenv

load_dotenv(verbose=True)

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

mysql = {
    "DB_USER": os.environ.get("DB_USER"),
    "DB_PORT": os.environ.get("DB_PORT"),
    "DB_HOST": os.environ.get("DB_HOST"),
    "DB_NAME": os.environ.get("DB_NAME"),
    "DB_PASSWORD": os.environ.get("DB_PASSWORD")
}
